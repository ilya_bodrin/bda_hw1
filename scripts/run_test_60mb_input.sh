#!/bin/bash
export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:target/hw1-0.0.1-SNAPSHOT.jar
hadoop fs -rm -r -f /input /output
hadoop fs -mkdir -p /input
hadoop fs -put input/input_60mb.txt  /input/
hadoop jar target/hw1-0.0.1-SNAPSHOT.jar mephi.bda.hw1.LongestWord /input /output

echo -e '\n\n\n'
echo '-------------OUTPUT FILES:-------------'
hadoop fs -ls /output/

echo -e '\n\n\n'
echo '-------------RAW OUTPUT DATA (SEQUENCE FILE - FIRST REDUCER):-------------'
hadoop fs -cat /output/part-r-00000
echo '-------------RAW OUTPUT DATA (SEQUENCE FILE - SECOND REDUCER):-------------'
hadoop fs -cat /output/part-r-00001

echo -e '\n\n\n'
echo '-------------TEXT OUTPUT DATA (FIRST REDUCER):-------------'
hadoop fs -text /output/part-r-00000
echo -e '\n\n\n'
echo '-------------TEXT OUTPUT DATA (SECOND REDUCER):-------------'
hadoop fs -text /output/part-r-00001


