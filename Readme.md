## Task
Program that:

* Searches for the longest word in the txt files
* Only words with ASCII characters
* Is using a custom type (Word)
* Has more than one reducers (2)

## Deployment steps
Prepare docker containers:

```bash
sudo docker pull kiwenlau/hadoop:1.0
git clone https://github.com/kiwenlau/hadoop-cluster-docker
sudo docker network create --driver=bridge hadoop
cd hadoop-cluster-docker
sudo ./start-container.sh
./start-hadoop.sh
apt -y install maven git
```

Clone and build project:
```bash
git clone https://ilya_bodrin@bitbucket.org/ilya_bodrin/bda_hw1.git
cd bda_hw
mvn clean install
```
Prepare input data:
```bash
for i in {1..10} ; do cat scripts/input.txt >> input/input_60mb.txt ; done
for i in {1..100} ; do cat scripts/input.txt >> input/input_600mb.txt ; done
```

Run test script (60 mb input file):
```bash
chmod 777 ./scripts/run_test_60mb_input.sh
./scripts/run_test_60mb_input.sh
```

Run test script (600 mb input file):
```bash
chmod 777 ./scripts/run_test_600mb_input.sh
./scripts/run_test_600mb_input.sh
```

## Screenshots

#### Maven install and tests
![picture](screenshots/install.png)

#### Results (600mb input file - job output):
![picture](screenshots/600m_1.png)
![picture](screenshots/600m_2.png)
![picture](screenshots/600m_3.png)

#### Results (600mb input file - system performance):
## Before test:
![picture](screenshots/600m_test_screenshot_1.png)

## Map 0%:
![picture](screenshots/600m_test_screenshot_2.png)

## Map 15%:
![picture](screenshots/600m_test_screenshot_3.png)

## Map 37%:
![picture](screenshots/600m_test_screenshot_4.png)

## Map 51%:
![picture](screenshots/600m_test_screenshot_5.png)

## Map 67%:
![picture](screenshots/600m_test_screenshot_6.png)

## After the test:
![picture](screenshots/600m_test_screenshot_9.png)