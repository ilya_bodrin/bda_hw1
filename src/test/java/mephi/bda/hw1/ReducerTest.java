package mephi.bda.hw1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.*;
import java.io.IOException;
import java.util.*;

public class ReducerTest {
	
	private LongestWord.LongestWord_Reducer reducer;
	ArrayList<Word> mapperOutputValues1, mapperOutputValues2;
	
    @Before
    public void initTest() {
        reducer = new LongestWord.LongestWord_Reducer();
        mapperOutputValues1 = new ArrayList<Word>();
        mapperOutputValues1.add(new Word(new Text("asomefirstword")));
        
        mapperOutputValues2 = new ArrayList<Word>();
        mapperOutputValues2.add(new Word(new Text("aasomefirstword")));        
    }

    @After
    public void afterTest() {
        reducer = null;
        mapperOutputValues1.clear();
        mapperOutputValues2.clear();
        System.gc();
    }

    @Test
    public void testReduce() throws IOException {
        ReduceDriver<LongWritable, Word, Text, Word> driver = ReduceDriver.newReduceDriver(reducer);
        //driver.withInput(new LongWritable(14), mapperOutputValues1);
        driver.withInput(new LongWritable(15), mapperOutputValues2);      
        driver.withOutput(new Text("The word of length 15 is found:"), new Word(new Text("aasomefirstword")));	        
        driver.runTest();
    }
}
