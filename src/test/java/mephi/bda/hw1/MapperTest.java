package mephi.bda.hw1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.*;
import java.io.IOException;
import java.util.*;

public class MapperTest {
	
	private LongestWord.LongestWord_TokenizerMapper mapper;
	
    @Before
    public void initTest() {
        mapper = new LongestWord.LongestWord_TokenizerMapper();
    }

    @After
    public void afterTest() {
        mapper = null;
        System.gc();
    }

    @Test
    public void testMap	() throws IOException {
    	MapDriver<Object,Text,LongWritable,Word> driver = MapDriver.newMapDriver(mapper);
    	driver.withInput(NullWritable.get(), new Text("The longest word is devastation"));
    	driver.withInput(NullWritable.get(), new Text("-----------------------------------------------"));
    	driver.withOutput(new LongWritable(11), new Word(new Text("devastation")));
        driver.runTest();
    }
}
