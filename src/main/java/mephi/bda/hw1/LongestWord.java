package mephi.bda.hw1;

import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer; 
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class LongestWord {
	
  public static class LongestWord_TokenizerMapper extends Mapper<Object, Text, LongWritable, Word>{
	private LongWritable maxLength = new LongWritable(0);
	
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString());
      Set<String> longestWords = new HashSet<String>();      
      while (itr.hasMoreTokens()) {
          String token=itr.nextToken();
          boolean isAscii = token.matches("\\A\\p{ASCII}*\\z");
          boolean hasLetters = token.matches("[A-z]+[0-9]{0,}");
          if (!isAscii || !hasLetters)
          	continue;
          if (token.length() > maxLength.get()) {
        	  maxLength.set(token.length());
        	  longestWords.clear();
        	  longestWords.add(token);
          } else if (token.length() == maxLength.get()) {
        	  longestWords.add(token);
          }
      }
      for (String s : longestWords) {
    	  context.write(maxLength, new Word(new Text(s)));
      }
    }
    
  }

  public static class LongestWord_Reducer extends Reducer<LongWritable, Word, Text, Word> {
	  private HashSet<String> longestWords = new HashSet<String>();
	  public void reduce(LongWritable key, Iterable<Word> values, Context context) throws IOException, InterruptedException {
		  for (Word w : values)
			  longestWords.add(w.toString());
		  for (String s : longestWords)
			  context.write(new Text("The word of length " + key + " is found:"), new Word(new Text(s)));
	  }

	@Override
	public void run(Context context) throws IOException, InterruptedException {
		super.setup(context);
		while(context.nextKey()) {
		    reduce(context.getCurrentKey(), context.getValues(), context);
		    break;
		}
		cleanup(context);
	}
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "Longest word");
    job.setMapOutputKeyClass(LongWritable.class);
    job.setMapOutputValueClass(Word.class);
    job.setJarByClass(LongestWord.class);
    job.setMapperClass(LongestWord_TokenizerMapper.class);
    job.setReducerClass(LongestWord_Reducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Word.class);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    job.setNumReduceTasks(2);
    job.setSortComparatorClass(LongWritable.DecreasingComparator.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}