package mephi.bda.hw1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;


public class Word implements WritableComparable<Word> {
	public Text word;
	public int length;
	
	public Word() {}	
	public Word(Text word) {
		this.word=word;
		this.length=word.getLength();
	}
	
	public String toString() {
		return word.toString();
	}
	
	public int toInt() {
		return this.length;
	}
	
	public void write(DataOutput out) throws IOException {
	    out.writeUTF(word.toString());
	}
	
	public void readFields(DataInput in) throws IOException {
	    word = new Text(in.readUTF());
	    //length = in.readInt();
	}
	
	public int getLength() {
		return this.length;
	}
	
	public int compareTo(Word wi){
		int thisLength=this.length;
		int thatLength=wi.length;
		if (wi.word.equals(this.word))
			return 0;
		else if (wi.toString().length() > this.word.toString().length())
			return 1;
		else
			return -1;
		//return (thisLength < thatLength ? -1 : (thisLength==thatLength ? 0 : 1));
		//return 0;
	}
	@Override
	public int hashCode() {
	    //return super.hashCode();
		return 1;
	}
	
	@Override
	public boolean equals(Object o) {
		return o.toString().equals(this.word.toString());
	}

}